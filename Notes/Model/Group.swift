//
//  Group.swift
//  Notes
//
//  Created by Filip Klembara on 10/04/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

import Foundation
import RealmSwift

public class Group: Object {
    @objc public dynamic var name = ""
    public dynamic let notes = List<Note>()
    @objc public dynamic var id = UUID().uuidString
    
    public override static func primaryKey() -> String? {
        return "id"
    }
}
