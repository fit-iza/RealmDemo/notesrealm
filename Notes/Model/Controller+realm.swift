//
//  Controller+realm.swift
//  Notes
//
//  Created by Filip Klembara on 11/04/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

extension UIViewController {
    func getRealm() -> Realm? {
        guard let realm = try? Realm() else {
            alert(title: "Realm error", message: "Can't get realm")
            return nil
        }
        return realm
    }
    
    func alert(title: String, message: String) {
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        ac.addAction(ok)
        present(ac, animated: true, completion: nil)
    }
    
    func performRealmAction<R>(action: () throws -> R?) -> R?{
        do {
            return try action()
        } catch let err {
            alert(title: "Realm action error", message: err.localizedDescription)
            return nil
        }
    }
}
