//
//  Note.swift
//  Notes
//
//  Created by Filip Klembara on 06/03/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

import Foundation
import RealmSwift

public class Note: Object {
    @objc public dynamic var date = Date()
    @objc public dynamic var text = ""
    public let groups = LinkingObjects(fromType: Group.self, property: "notes")
}
