//
//  GroupsTableViewController.swift
//  Notes
//
//  Created by Filip Klembara on 10/04/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

import UIKit
import RealmSwift

class GroupsTableViewController: UITableViewController {

    static let noteDetailSegue = "NoteDetail"

    var model: Results<Group>!
    
    var notificationToken: NotificationToken!

    override func viewDidLoad() {
        super.viewDidLoad()
        clearsSelectionOnViewWillAppear = false
        navigationItem.rightBarButtonItems?.append(editButtonItem)
        guard let realm = getRealm() else {
            fatalError()
        }
        model = realm.objects(Group.self)
        notificationToken = model.observe { [weak self] change in
            guard let tableView = self?.tableView else {
                return
            }
            switch change {
            case .initial(_):
                tableView.reloadData()
            case .update(_, let deletions, let insertions, let modifications):
                tableView.beginUpdates()
                
                tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
                                     with: .automatic)
                tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                
                tableView.endUpdates()
            case .error(let err):
                self?.alert(title: "Realm update error", message: "\(err)")
            @unknown default:
                self?.alert(title: "Realm update error", message: "Unknown change")
            }
        }
    }
    
    deinit {
        notificationToken.invalidate()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == GroupsTableViewController.noteDetailSegue,
            let vc = segue.destination as? NotesTableViewController,
            let cell = sender as? UITableViewCell,
            let indexPath = tableView.indexPath(for: cell) else {

                return
        }
        
        vc.id = model[indexPath.row].id
    }
    
    @IBAction func newGroup(_ sender: UIBarButtonItem) {
        let av = UIAlertController(title: "New group", message: nil, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let ok = UIAlertAction(title: "Create", style: .default) { _ in
            guard let name = av.textFields?.first?.text, name != "" else {
                return
            }
            
            self.performRealmAction {
                try self.model.realm?.write {
                    let group = Group()
                    group.name = name
                    self.model.realm?.add(group)
                }
            }
        }
        av.addTextField { (field) in
            field.placeholder = "Your great new group name!"
        }
        av.addAction(cancel)
        av.addAction(ok)
        present(av, animated: true, completion: nil)
    }
    
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupCell", for: indexPath)
        let group = model[indexPath.row]
        cell.detailTextLabel?.text = group.notes.count.description

        cell.textLabel?.text = group.name
        
        return cell
    }
    
    // Override to support conditional editing of the table view.
    public override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    
    // Override to support editing the table view.
    public override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            performRealmAction {
                try model.realm?.write {
                    model.realm?.delete(model[indexPath.row].notes)
                    model.realm?.delete(model[indexPath.row])
                }
            }
        } else if editingStyle == .insert {
            // nope
        }
    }
}
