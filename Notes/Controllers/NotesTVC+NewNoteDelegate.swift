//
//  NotesTVC+NewNoteDelegate.swift
//  Notes
//
//  Created by Filip Klembara on 11/03/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

import Foundation

extension NotesTableViewController: NewNoteDelegate {
    func saveNew(note text: String) {
        performRealmAction {
            let note = Note()
            note.text = text
            
            try model.realm?.write {
                model.realm?.add(note)
                guard let group = model.realm?.object(ofType: Group.self, forPrimaryKey: id) else {
                    return
                }
                group.notes.append(note)
            }
        }
    }
}
