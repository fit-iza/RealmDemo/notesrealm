//
//  NotesTableViewController.swift
//  Notes
//
//  Created by Filip Klembara on 06/03/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

import UIKit
import RealmSwift

class NotesTableViewController: UITableViewController {

    var model: Results<Note>!
    var id = ""
    private struct Segues {
        private init() { }
        static let newNoteSegue = "NewNoteSegue"
    }
    
    var notificationToken: NotificationToken!

    override func viewDidLoad() {
        super.viewDidLoad()

        clearsSelectionOnViewWillAppear = false
        navigationItem.rightBarButtonItems?.append(editButtonItem)
        
        guard let realm = getRealm() else {
            fatalError()
        }
        model = realm.objects(Note.self).filter("ANY groups.id = %@", id).sorted(byKeyPath: "date", ascending: false)
        
        notificationToken = model.observe { [weak self] change in
            guard let tableView = self?.tableView else {
                return
            }
            switch change {
            case .initial(_):
                tableView.reloadData()
            case .update(_, let deletions, let insertions, let modifications):
                tableView.beginUpdates()
                
                tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
                                     with: .automatic)
                tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                
                tableView.endUpdates()
            case .error(let err):
                self?.alert(title: "Realm update error", message: "\(err)")
            @unknown default:
                self?.alert(title: "Realm update error", message: "Unknown change")
            }
        }
    }
    
    deinit {
        notificationToken.invalidate()
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == Segues.newNoteSegue,
            let vc = segue.destination as? NewNoteViewController else {

                return
        }
        vc.delegate = self
    }
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoteCell", for: indexPath)
        let note = model[indexPath.row]
        cell.detailTextLabel?.text = note.text
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd,yyyy"
        
        cell.textLabel?.text = dateFormatter.string(from: note.date)
        
        return cell
    }
    
    // Override to support conditional editing of the table view.
    public override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    
    // Override to support editing the table view.
    public override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            performRealmAction {
                model.realm?.beginWrite()
                model.realm?.delete(model[indexPath.row])
                try model.realm?.commitWrite(withoutNotifying: [notificationToken])
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        } else if editingStyle == .insert {
            // nope
        }
    }
}
